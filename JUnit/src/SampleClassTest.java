import java.lang.reflect.Array;

import static org.junit.jupiter.api.Assertions.*;

class SampleClassTest {

    SampleClass sample = new SampleClass();
    SampleClass sample1 = new SampleClass();
    @org.junit.jupiter.api.Test
    public void main(){
        assertNotNull(sample);
        assertSame(sample, sample);
        assertNotSame(sample1, sample);
        int[] testNums = {0, 1, 2, 3};
        assertArrayEquals(sample.nums, testNums);
        String[] testStrings = {"Hi", "there"};
        assertArrayEquals(sample.strings, testStrings);
        assertEquals(sample.num, 0);
        assertEquals(sample.string, "Hello");
        assertTrue(sample.bool);


    }
}