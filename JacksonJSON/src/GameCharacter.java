import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.File;
import java.io.IOException;

public class GameCharacter {
    int health = 50;
    int rifleAmmo = 34;
    int rifleUpgradeLvl = 3;
    String checkpoint = new String("Haunted Mansion");
    String armor = new String("Glass Armor");
    String companion = new String("Samantha");
    static File save0 = new File("Save.json");


    public static void main(String[] args) {
        GameCharacter character = new GameCharacter();
        System.out.println(character.toString());
        save(character);
        System.out.println(save0);
        GameCharacter newCharacter = load();
        System.out.println(newCharacter);
    }

    private static void save(GameCharacter saveData){
        ObjectMapper map = new ObjectMapper();
        try {
            map.writeValue(save0, saveData);
        } catch (JsonGenerationException e){
            System.out.print("Could not convert to JSON" + e);
        } catch (JsonMappingException r){
            System.out.print("Could not convert to JSON" + r);
        } catch (IOException t){
            System.out.print("Could not convert to JSON:" + t);
        }
    }

    private static GameCharacter load(){
        GameCharacter character = new GameCharacter();
        ObjectMapper map = new ObjectMapper();
        try{
            character = map.readValue(save0, GameCharacter.class);
        }catch (JsonParseException e){
            System.out.print("Could not load JSON" + e);
        }catch (JsonMappingException e){
            System.out.print("Could not load JSON" + e);
        } catch (IOException e) {
            System.out.print("Could not load JSON" + e);
        }

        return character;
    }

    @Override
    public String toString() {
        return "GameCharacter{" +
                "health=" + health +
                ", rifleAmmo=" + rifleAmmo +
                ", rifleUpgradeLvl=" + rifleUpgradeLvl +
                ", checkpoint='" + checkpoint + '\'' +
                ", armor='" + armor + '\'' +
                ", companion='" + companion + '\'' +
                '}';
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getRifleAmmo() {
        return rifleAmmo;
    }

    public void setRifleAmmo(int rifleAmmo) {
        this.rifleAmmo = rifleAmmo;
    }

    public int getRifleUpgradeLvl() {
        return rifleUpgradeLvl;
    }

    public void setRifleUpgradeLvl(int rifleUpgradeLvl) {
        this.rifleUpgradeLvl = rifleUpgradeLvl;
    }

    public String getCheckpoint() {
        return checkpoint;
    }

    public void setCheckpoint(String checkpoint) {
        this.checkpoint = checkpoint;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    public String getCompanion() {
        return companion;
    }

    public void setCompanion(String companion) {
        this.companion = companion;
    }
}

