
import java.util.*;

public class Run {

    public static void main(String[] args) {
        TestDAO t = TestDAO.getInstance();

        System.out.println("Get all people");

        List<Persons> x = t.getPersons();
        for(Persons i : x) {
            System.out.println(i);
        }

        System.out.println(" \nGet the first person");
        System.out.println(t.getPerson(1));

        System.out.println(" \nGet all the people in Central City");
        System.out.println(t.getCity("Central"));
    }
}
