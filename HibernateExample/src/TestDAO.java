
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }


    public List<Persons> getPersons() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Persons";
            List<Persons> ps = (List<Persons>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return ps;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Persons getPerson(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Persons where PersonId=" + Integer.toString(id);
            Persons p = (Persons)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return p;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Persons> getCity(String City) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Persons where City='" + City + "'";
            List<Persons> p = (List<Persons>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return p;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
