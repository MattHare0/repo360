import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunTimer {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(2);

        Timer cookies = new Timer(10,"Cookies", "The cookies are done baking.");
        Timer bread = new Timer(15,"Bread", "The bread is done rising.");
        Timer washer = new Timer(7,"Clothes", "The washer is done.");
        Timer dryer = new Timer(8,"Clothes", "The dryer is finished.");

        myService.execute(cookies);
        myService.execute(bread);
        myService.execute(washer);
        myService.execute(dryer);
        myService.execute(cookies);


        myService.shutdown();
    }
}
