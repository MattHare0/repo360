public class Timer implements Runnable{
    public int time;
    public String name;
    public String message;

    public Timer(int time, String name, String message) {
        this.time = time;
        this.name = name;
        this.message = message;
    }

    public void run() {
        System.out.println("Starting " + name + " Timer");
        for (int count = 1; count < time; count++) {
            System.out.print(name + " Timer is " + count + "/" + time + " ");
                try {
                    Thread.sleep(100 * time);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }

        }
        System.out.println("\n Timer " + name + " is finished. " + message);
    }
}
