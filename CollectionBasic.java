import java.util.*;

public class CollectionBasic {

    public static void main(String[] args){
        System.out.println("---Array---");
        int a[] = new int[] {1, 2, 3, 4, 1};

        for (int var: a)
        {
            System.out.println(var);
        }

        System.out.println("---Hashtable---");
        Hashtable<Integer, String> hash = new Hashtable();
        hash.put(1, "First");
        hash.put(2, "Second");
        hash.put(3, "Third");

        //Lookup with the Key
        for(int i=1;i<4;i++)
        {
            System.out.println(hash.get(i));
        }

        System.out.println("---Queue---");
        Queue<Integer> q = new LinkedList<>();
        q.add(5);
        q.add(4);
        q.add(3);
        q.add(2);
        q.add(1);

        System.out.println(q);
        int firstOut = q.remove();
        System.out.println("Removed: " + firstOut);
        System.out.println(q);
        //Uses FIFO

        System.out.println("---Set---");
        Set<Integer> set = new HashSet<Integer>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(1);

        System.out.println(set);
        //missing the duplicate
    }
}
