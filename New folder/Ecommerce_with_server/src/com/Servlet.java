package com;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DAO dao = DAO.getInstance();
        List<Inventory> stock = dao.getInventory();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h1>Dex's Dinner</h1>");
        out.println("<h2>Inventory</h2><ul>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getItem_name() + "      " + i.getItem_description() + "      " + i.getPrice() + " Credits" + "      " + i.getStock() + "</li>");

        }
        out.println("</ul>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h1>Dex's Dinner</h1>");
        out.println("</body></html>");

    }


}
