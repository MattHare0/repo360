package com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class DAO {

    SessionFactory factory = null;
    Session session = null;

    private static DAO single_instance = null;

    private DAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static DAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new DAO();
        }

        return single_instance;
    }

    public List<Inventory> getInventory() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Inventory";
            List<Inventory> i = (List<Inventory>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return i;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    public Customer checkCustomer(int code) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Customer where customer_code = " + code;
            Customer c = (Customer)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }


}
