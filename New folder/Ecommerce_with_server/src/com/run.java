package com;

import java.util.List;

public class run {

    public static void main(String[] args) {
        DAO dao = DAO.getInstance();

        Customer c1 = dao.checkCustomer(1001);

        System.out.println(c1.toString());

        List<Inventory> stock = dao.getInventory();

        for (Inventory i : stock) {
            System.out.println(i.item_name);
        }

    }
}
