package com;

import javax.persistence.*;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @Column(name = "customer_code")
    int customer_code;

    @Column(name = "customer_name")
    String customer_name;

    @Column(name = "customer_address")
    String customer_address;

    @Column(name = "customer_standing")
    String customer_standing;

    public int getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(int customer_code) {
        this.customer_code = customer_code;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_standing() {
        return customer_standing;
    }

    public void setCustomer_standing(String customer_standing) {
        this.customer_standing = customer_standing;
    }

    @Override
    public String toString() {
        return "org.Customer{" +
                "customer_code=" + customer_code +
                ", customer_name='" + customer_name + '\'' +
                ", customer_address='" + customer_address + '\'' +
                ", customer_standing='" + customer_standing + '\'' +
                '}';
    }
}
