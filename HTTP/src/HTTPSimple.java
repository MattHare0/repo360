import java.io.*;
import java.net.*;
import java.util.*;

public class HTTPSimple {
    public static void main(String[] args) {
        //Content
        String website = "https://www.cdc.gov";

        try{
            URL url = new URL(website);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder= new StringBuilder();

            String line = "";
            while ((line=reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }

            System.out.println(stringBuilder.toString());

            //headers
            System.out.println(http.getHeaderFields());
        }catch (Exception e){
            System.out.println(e.toString());
        }
    }

}
