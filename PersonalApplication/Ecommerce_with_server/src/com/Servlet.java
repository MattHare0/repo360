package com;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DAO dao = DAO.getInstance();
        List<Inventory> stock = dao.getInventory();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<html><head><title>Dex's Diner</title>");

        out.println("</head><body>");
        out.println("<form action='BuyServlet' method='post'>");
        out.println("<h1>Dex's Diner</h1>");
        out.println("<h2>Inventory</h2><ul>");

        out.println("<div style='float: left; width: 10%;'><ul style='list-style: none'> <h3>Item</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getItem_name() + "<br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 20%;'><ul style='list-style: none'> <h3>Description</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getItem_description() + "<br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 10%;'><ul style='list-style: none'> <h3>Price</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getPrice() + " Credits <br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 15%;'><ul style='list-style: none'> <h3>Number in stock</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getStock() + "<br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 15%;'><ul style='list-style: none'><h3>Number to purchase</h3>");

        for (int i = 0; i < stock.size() ; i++)
        {
            out.println("<li><input name='" + i + "' type='number' style='padding: 0px;margin: 0px'/></li><br>");
        }
        out.println("</div>");
        out.println("<div style='float: left; width: 10%;'><h3>Customer code</h3>");
        out.println("<input name='code' type='number'><br><br>");
        out.println("<input type='submit' value='Purchase'");
        out.println("</div></ul>");


        out.println("</form>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        out.println("<h1>Dex's Dinner</h1>");
        out.println("</body></html>");

    }


}
