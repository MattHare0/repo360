package com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;




public class SubDAO implements Runnable{

    SessionFactory factory = null;
    Session session = null;
    String name;
    int sub;
    boolean running = true;

    private SubDAO single_instance = null;

    public SubDAO(String name, int sub) {
        factory = HibernateUtils.getSessionFactory();
        this.name = name;
        this.sub = sub;
    }


    public void run(){
        try {
            //Query query = session.createQuery("update Inventory set stock = stock -" + sub + " where item_name = " + name);


            System.out.println("Subtracting");
            session = factory.openSession();
            session.getTransaction().begin();
            session.createQuery("update Inventory set stock = stock -" + sub + " where item_name = '" + name + "'").executeUpdate();
            session.getTransaction().commit();
            System.out.println("Done Subtracting");
        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return;
        } finally {
            session.close();
        }
        return;
    }


}
