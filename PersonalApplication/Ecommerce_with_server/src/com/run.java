package com;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class run {

    public static void main(String[] args) {
        DAO dao = DAO.getInstance();
        SubDAO sdao = new SubDAO("Caf", 1);
        Customer c1 = dao.checkCustomer(1001);

        ExecutorService myService = Executors.newFixedThreadPool(4);
        myService.execute(sdao);

        System.out.println(c1.toString());

        List<Inventory> stock = dao.getInventory();

        for (Inventory i : stock) {
            System.out.println(i.item_name + i.getStock());
        }

        return;
    }

}
