package com;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.Date;

@WebServlet(name = "BuyServlet", urlPatterns = {"/BuyServlet"})
public class BuyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Buy the goods
        DAO dao = DAO.getInstance();
        List<Inventory> stock = dao.getInventory();

        if (request.getParameter("code") == "")
        {
            return;
        }
        int userCode = Integer.parseInt(request.getParameter("code"));
        Customer cust = dao.checkCustomer(userCode);
        boolean accepted = false;

        if (cust != null)
        {
            //subtract the inventory
            accepted = true;
            ExecutorService myService = Executors.newFixedThreadPool(6);
            ArrayList<Integer> order = new ArrayList<Integer>();

            for (int i=0; i < stock.size(); i++){
                if (request.getParameter(Integer.toString(i)) == "")
                {
                    order.add(0);
                }
                else {
                    order.add(Integer.parseInt(request.getParameter(Integer.toString(i))));
                }
            }


            for (int i=0; i < order.size(); i++)
            {

                if (order.get(i) != 0)
                {

                    SubDAO sub = new SubDAO(stock.get(i).getItem_name(), order.get(i));
                    myService.execute(sub);
                }
            }

            //CountDownLatch latch = new CountDownLatch(numThreads);

        }

        try{
            TimeUnit.SECONDS.sleep(1);
        }catch (Exception e){
            System.out.println(e.toString());
        }

        stock = dao.getInventory();

        //Print out the results



        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<html><head><title>Dex's Diner</title>");

        out.println("</head><body>");
        out.println("<form action='BuyServlet' method='post'>");
        out.println("<h1>Dex's Diner</h1>");
        out.println("<h2>Inventory</h2><ul>");

        out.println("<div style='float: left; width: 10%;'><ul style='list-style: none'> <h3>Item</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getItem_name() + "<br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 20%;'><ul style='list-style: none'> <h3>Description</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getItem_description() + "<br><br></li>");
        }
        if (accepted)
        {
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            out.println("<h3>Your order at " + formatter.format(date.getTime()) + " has been completed</h3>");
            out.println("<p> Your order will be shipped to: <br>" + cust.getCustomer_name() + "<br>" + cust.getCustomer_address() + "</p>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 10%;'><ul style='list-style: none'> <h3>Price</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getPrice() + " Credits <br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 15%;'><ul style='list-style: none'> <h3>Number in stock</h3>");
        for (Inventory i : stock)
        {
            out.println("<li>" + i.getStock() + "<br><br></li>");
        }
        out.println("</ul></div>");

        out.println("<div style='float: left; width: 15%;'><ul style='list-style: none'><h3>Number to purchase</h3>");

        for (int i = 0; i < stock.size() ; i++)
        {
            out.println("<li><input name='" + i + "' type='number' style='padding: 0px;margin: 0px'/></li><br>");
        }
        out.println("</div>");
        out.println("<div style='float: left; width: 10%;'><h3>Customer code</h3>");
        out.println("<input name='code' type='text'><br><br>");
        out.println("<input type='submit' value='Purchase'");
        out.println("</div></ul>");


        out.println("</form>");
        out.println("</body></html>");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
