package com.test;

import org.junit.jupiter.api.Test;
import com.DAO;

import static org.junit.jupiter.api.Assertions.*;

class DAOTest {

// test to see if the database works
    @Test
    void getInventory() {
        DAO dao = DAO.getInstance();
        assertEquals(dao.getInventory().get(0).getItem_name(), "Astromech droid");
    }

    @Test
    void checkCustomer() {
        DAO dao = DAO.getInstance();
        assertEquals(dao.checkCustomer(1001).getCustomer_name(), "Anakin Skywalker");
    }
}